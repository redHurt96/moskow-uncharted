using System.Collections;
using DiasGames.Components;
using UnityEngine;
using UnityEngine.Rendering;
using static UnityEngine.Mathf;
using static UnityEngine.Time;

namespace _Project.Logic.Effects
{
    public class DeathSaturation : MonoBehaviour
    {
        [SerializeField] private Volume _volume;
        [SerializeField] private Health _health;
        [SerializeField] private float _duration;

        private void Start()
        {
            _volume.weight = 0f;

            _health.OnDead += ChangeSaturation;
        }

        private void ChangeSaturation(DeathReason deathReason)
        {
            StartCoroutine(FadeOut());
        }

        private IEnumerator FadeOut()
        {
            float time = 0f;

            while (time < _duration)
            {
                _volume.weight = Lerp(0f, 1f, time / _duration);
                time += deltaTime;

                yield return null;
            }
        }
    }
}
