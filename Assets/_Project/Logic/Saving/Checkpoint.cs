using _Project.Logic.Level;
using DiasGames.Controller;
using NaughtyAttributes;
using UnityEngine;
using static System.Guid;
using static _Project.Logic.Saving.SaveManager;

#if UNITY_EDITOR
using static UnityEditor.EditorUtility;
#endif

namespace _Project.Logic.Saving
{
    public class Checkpoint : CharacterTrigger
    {
        public Vector3 SpawnPosition => _spawnPoint.position;
        public string Id => _id;

        [SerializeField] private string _id;
        [SerializeField] private Transform _spawnPoint;
        
        protected override void ExecuteOnEnter(CSPlayerController csPlayerController) => 
            Save(_id);
        
#if UNITY_EDITOR
        [Button]
        private void AssignIds()
        {
            foreach (Checkpoint checkpoint in FindObjectsOfType<Checkpoint>())
            {
                checkpoint._id = NewGuid().ToString();
                SetDirty(checkpoint);
            }
        }

        [Button]
        private void SetAsCurrent() => 
            Save(_id);
#endif
    }
}