using _Project.Logic.Level;
using DiasGames.Controller;
using UnityEngine;
using static _Project.Logic.Saving.SaveManager;

namespace _Project.Logic.Saving
{
    public class CharacterLoader : MonoBehaviour
    {
        private void Awake()
        {
            if (HasSave)
            {
                CSPlayerController character = FindObjectOfType<CSPlayerController>();
                Checkpoint point = GetCheckpoint();

                character.transform.position = point.SpawnPosition;
            }
        }
    }
}