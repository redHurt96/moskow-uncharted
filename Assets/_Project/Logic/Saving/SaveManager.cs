using System.Linq;
using _Project.Logic.Level;
using UnityEditor;
using UnityEngine;
using static UnityEngine.Object;
using static UnityEngine.PlayerPrefs;

namespace _Project.Logic.Saving
{
    public static class SaveManager
    {
        private const string CHECKPOINT_KEY = "Checkpoint id";
        
        public static bool HasSave => 
            HasKey(CHECKPOINT_KEY);

#if UNITY_EDITOR
        [MenuItem("🎮 Game/🧹💾 Clear save")]
#endif
        public static void ClearSave() =>
            DeleteKey(CHECKPOINT_KEY);

        public static void Save(string id)
        {
            SetString(CHECKPOINT_KEY, id);
            PlayerPrefs.Save();
        }

        public static Checkpoint GetCheckpoint() => 
            FindObjectsOfType<Checkpoint>()
                .First(x => x.Id == GetString(CHECKPOINT_KEY));
    }
}