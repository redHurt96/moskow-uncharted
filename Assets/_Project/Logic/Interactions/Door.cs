using DG.Tweening;
using UnityEngine;
using static DG.Tweening.Ease;

namespace _Project.Logic.Interactions
{
    internal class Door : MonoBehaviour
    {
        [SerializeField] private Vector3 _shift;
        [SerializeField] private float _duration;
        
        private Tween _tween;

        private void OnDestroy() => 
            _tween?.Kill();

        public void Open() => 
            _tween = transform
                .DOLocalMove(transform.localPosition + _shift, _duration)
                .SetEase(InOutSine);
    }
}