using System;
using DG.Tweening;
using UnityEngine;
using static UnityEngine.Mathf;

namespace _Project.Logic.Interactions
{
    public class TurningRiddleElement : Interactable
    {
        public event Action RotationCompleted;
        
        public override bool Disabled { get; protected set; }
        public override bool Blocked { get; protected set; }
        public bool IsTurnedCorrect => Abs(transform.localRotation.eulerAngles.y - _targetAngle) < .1f;

        [SerializeField] private float _rotationAngle;
        [SerializeField] private float _rotationDuration;
        [SerializeField] private float _targetAngle;
        
        private Tween _tween;

        protected override void ExecuteOnInteract()
        {
            Blocked = true;

            _tween = transform
                .DOLocalRotate(new(0f, transform.localRotation.eulerAngles.y + _rotationAngle, 0f), _rotationDuration)
                .OnComplete(() =>
                {
                    RotationCompleted?.Invoke();
                    Blocked = false;
                });
        }

        public void Clear()
        {
            _tween?.Kill();
            
            Disabled = true;
        }
    }
}