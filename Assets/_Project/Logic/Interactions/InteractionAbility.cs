using UnityEngine;
using static UnityEngine.LayerMask;
using static UnityEngine.Physics;

namespace _Project.Logic.Interactions
{
    public class InteractionAbility : MonoBehaviour
    {
        [SerializeField] private Transform _camera;
        [SerializeField] private float _distance;
        [SerializeField] private InteractionUI _interactionUI;
        [SerializeField] private LayerMask _layerMask;
        
        private Interactable _currentInteractable;

        private void Update()
        {
#if UNITY_EDITOR
            Debug.DrawLine(_camera.position, _camera.position + _camera.forward * _distance, Color.red);
#endif
            
            if (Raycast(_camera.position, _camera.forward, out RaycastHit hitInfo, _distance, _layerMask)
                && hitInfo.transform.TryGetComponent(out Interactable interactable))
            {
                _currentInteractable = interactable;
                _interactionUI.Show(interactable);
            }
            else
            {
                _currentInteractable = null;
                _interactionUI.Hide();
            }
        }

        private void OnInteract()
        {
            _currentInteractable?.Interact();
        }
    }
}
