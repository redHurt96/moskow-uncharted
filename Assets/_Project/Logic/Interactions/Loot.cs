using System;

namespace _Project.Logic.Interactions
{
    public class Loot : Interactable
    {
        public event Action PickedUp;
        
        public override bool Disabled { get; protected set; }
        public override bool Blocked { get; protected set; }
        
        protected override void ExecuteOnInteract()
        {
            PickedUp?.Invoke();
            
            Destroy(gameObject);
        }
    }
}