using TMPro;
using UnityEngine;

namespace _Project.Logic.Interactions
{
    internal class InteractionUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _title;
        [SerializeField] private TextMeshProUGUI _tip;

        public void Show(Interactable interactable)
        {
            if (interactable.Disabled)
            {
                Hide();
            }
            else
            {
                _title.text = interactable.Title;
                _tip.text = interactable.Blocked
                    ? string.Empty
                    : interactable.Tip;
            }
        }

        public void Hide()
        {
            _title.text = string.Empty;
            _tip.text = string.Empty;
        }
    }
}