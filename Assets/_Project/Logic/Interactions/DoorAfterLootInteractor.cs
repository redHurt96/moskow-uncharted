using UnityEngine;

namespace _Project.Logic.Interactions
{
    public class DoorAfterLootInteractor : MonoBehaviour
    {
        [SerializeField] private Loot _loot;
        [SerializeField] private Door _door;

        private void Start() => 
            _loot.PickedUp += _door.Open;

        private void OnDestroy() => 
            _loot.PickedUp -= _door.Open;
    }
}