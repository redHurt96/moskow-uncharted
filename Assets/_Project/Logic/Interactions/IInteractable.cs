using UnityEngine;

namespace _Project.Logic.Interactions
{
    public abstract class Interactable : MonoBehaviour
    {
        public string Title => _title;
        public string Tip => _tip;
        
        public abstract bool Disabled { get; protected set; }
        public abstract bool Blocked { get; protected set; }

        [SerializeField] protected string _title;
        [SerializeField] protected string _tip;
        
        public void Interact()
        {
            if (Disabled || Blocked)
                return;

            ExecuteOnInteract();
        }

        protected abstract void ExecuteOnInteract();
    }
}