using System.Linq;
using UnityEngine;

namespace _Project.Logic.Interactions
{
    public class TurningRiddle : MonoBehaviour
    {
        [SerializeField] private TurningRiddleElement[] _elements;
        [SerializeField] private Door _door;

        private void Start()
        {
            foreach (TurningRiddleElement element in _elements)
                element.RotationCompleted += CheckElements;
        }

        private void OnDestroy()
        {
            foreach (TurningRiddleElement element in _elements)
                element.RotationCompleted -= CheckElements;
        }

        private void CheckElements()
        {
            if (_elements.All(x => x.IsTurnedCorrect))
            {
                foreach (TurningRiddleElement element in _elements)
                    element.Clear();
                
                _door.Open();
            }
        }
    }
}