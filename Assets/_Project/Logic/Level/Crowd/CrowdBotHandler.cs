using _Project.Logic.Bots;
using AI.FluentFiniteStateMachine;
using UnityEngine;

namespace _Project.Logic.Level.Transport
{
    public class CrowdBotHandler : MonoBehaviour
    {
        [SerializeField] private Bot _bot;
        
        private StateMachine _stateMachine;

        private void Start() =>
            _stateMachine = new StateMachine()
                .AddStates(
                    new PatrolState(_bot),
                    new WaitState(_bot))
                .AddTransitions(
                    new Transition(typeof(PatrolState), typeof(WaitState), _bot.ReachPoint),
                    new Transition(typeof(WaitState), typeof(PatrolState), _bot.WaitEnough))
                .Run();

        private void Update() => 
            _stateMachine.Update();
    }
}