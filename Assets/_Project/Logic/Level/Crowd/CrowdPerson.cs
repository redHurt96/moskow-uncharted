using UnityEngine;
using UnityEngine.AI;

namespace _Project.Logic.Level.Transport
{
    internal class CrowdPerson : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        
        private float _lifeTime;
        private float _bornTime;

        public void Setup(Transform movePoint, float lifeTime)
        {
            _lifeTime = lifeTime;
            _bornTime = Time.time;
            _navMeshAgent.SetDestination(movePoint.position);
        }

        private void Update()
        {
            if (_bornTime + _lifeTime < Time.time)
                Destroy(gameObject);
        }
    }
}