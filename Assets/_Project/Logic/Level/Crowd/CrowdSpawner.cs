using System;
using System.Collections;
using UnityEngine;
using static UnityEngine.Random;

namespace _Project.Logic.Level.Transport
{
    public class CrowdSpawner : MonoBehaviour
    {
        [SerializeField] private CrowdPerson[] _prefabs;
        [SerializeField] private Transform _movePoint;
        [SerializeField] private float _delayMin;
        [SerializeField] private float _delayMax;
        [SerializeField] private float _lifeTime;

        private int _lastIndex = -1;
        
        private IEnumerator Start()
        {
            while (gameObject.activeSelf)
            {
                GetIndex();
                
                CrowdPerson car = Instantiate(_prefabs[_lastIndex], transform.position, transform.rotation, transform);

                car.Setup(_movePoint, _lifeTime);
                
                yield return new WaitForSeconds(Range(_delayMin, _delayMax));
            }
        }

        private void GetIndex()
        {
            if (_lastIndex == -1)
            {
                _lastIndex = Range(0, _prefabs.Length);
            }
            else
            {
                int newIndex = _lastIndex;

                do newIndex = Range(0, _prefabs.Length);
                while (newIndex == _lastIndex);

                _lastIndex = newIndex;
            }
        }
    }
}