using DiasGames.Controller;
using EasyTransition;

namespace _Project.Logic.Level
{
    public class EndGameTrigger : CharacterTrigger
    {
        protected override void ExecuteOnEnter(CSPlayerController csPlayerController) =>
            FindObjectOfType<TransitionManager>()
                .EndGame(0, "RectangleGrid", 0f);
    }
}