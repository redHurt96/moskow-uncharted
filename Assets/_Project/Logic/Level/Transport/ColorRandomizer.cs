using UnityEngine;
using static UnityEngine.Random;

namespace _Project.Logic.Level.Transport
{
    [RequireComponent(typeof(MeshRenderer))]
    public class ColorRandomizer : MonoBehaviour
    {
        [SerializeField] private Color[] _colors;
        [SerializeField] private int _materialIndex;

        private void Start()
        {
            Material material = GetComponent<Renderer>().materials[_materialIndex];
            material.color = _colors[Range(0, _colors.Length)];
        }
    }
}