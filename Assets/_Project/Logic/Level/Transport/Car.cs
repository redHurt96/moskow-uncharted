using UnityEngine;

namespace _Project.Logic.Level.Transport
{
    public class Car : MonoBehaviour
    {
        private float _speed;
        private Transform _transform;
        private float _lifeTime;
        private float _bornTime;

        public void Setup(float speed, float lifeTime)
        {
            _lifeTime = lifeTime;
            _speed = speed;
            _bornTime = Time.time;
            _transform = transform;
        }

        private void Update()
        {
            _transform.position += _transform.forward * _speed * Time.deltaTime;

            if (_bornTime + _lifeTime < Time.time)
                Destroy(gameObject);
        }
    }
}