using System.Collections;
using UnityEngine;
using static UnityEngine.Random;

namespace _Project.Logic.Level.Transport
{
    public class CarSpawner : MonoBehaviour
    {
        [SerializeField] private Car[] _prefabs;
        [SerializeField] private float _delayMin;
        [SerializeField] private float _delayMax;
        [SerializeField] private float _speed;
        [SerializeField] private float _lifeTime;

        private IEnumerator Start()
        {
            while (gameObject.activeSelf)
            {
                Car car = Instantiate(_prefabs[Range(0, _prefabs.Length)], transform.position, transform.rotation, transform);

                car.Setup(_speed, _lifeTime);
                
                yield return new WaitForSeconds(Range(_delayMin, _delayMax));
            }
        }
    }
}