using DiasGames.Components;
using DiasGames.Controller;

namespace _Project.Logic.Level
{
    public class DeathZone : CharacterTrigger
    {
        protected override void ExecuteOnEnter(CSPlayerController csPlayerController) =>
            csPlayerController
                .GetComponent<Health>()
                .Damage(1000000);
    }
}
