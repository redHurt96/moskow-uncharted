using DiasGames.Controller;
using UnityEngine;

namespace _Project.Logic.Level
{
    public abstract class CharacterTrigger : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out CSPlayerController character))
                ExecuteOnEnter(character);
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.TryGetComponent(out CSPlayerController character))
                ExecuteOnExit(character);
        }

        protected abstract void ExecuteOnEnter(CSPlayerController csPlayerController);
        protected virtual void ExecuteOnExit(CSPlayerController csPlayerController) {}
    }
}