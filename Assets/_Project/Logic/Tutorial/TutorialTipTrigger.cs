using _Project.Logic.Level;
using DiasGames.Controller;
using UnityEngine;

namespace _Project.Logic.Tutorial
{
    public class TutorialTipTrigger : CharacterTrigger
    {
        [SerializeField] private string _text;
    
        private TutorialTip _tutorialTip;

        private void Start() => 
            _tutorialTip ??= FindObjectOfType<TutorialTip>();

        protected override void ExecuteOnEnter(CSPlayerController csPlayerController) => 
            _tutorialTip.Show(_text);

        protected override void ExecuteOnExit(CSPlayerController csPlayerController)
        {
            if (_tutorialTip.SameTipShown(_text))
                _tutorialTip.Hide();
        }
    }
}