using _Project.Logic.Level;
using DiasGames.Controller;

namespace _Project.Logic.Tutorial
{
    public class ZoomZoneTrigger : CharacterTrigger
    {
        protected override void ExecuteOnEnter(CSPlayerController csPlayerController) => 
            csPlayerController.Zoom = true;

        protected override void ExecuteOnExit(CSPlayerController csPlayerController) => 
            csPlayerController.Zoom = false;
    }
}