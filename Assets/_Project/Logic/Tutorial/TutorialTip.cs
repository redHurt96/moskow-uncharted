using TMPro;
using UnityEngine;
using static System.String;

namespace _Project.Logic.Tutorial
{
    internal class TutorialTip : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _tip;
        
        public void Show(string text) => 
            _tip.text = text;

        public void Hide() => 
            _tip.text = Empty;

        public bool SameTipShown(string text) => 
            _tip.text == text;
    }
}