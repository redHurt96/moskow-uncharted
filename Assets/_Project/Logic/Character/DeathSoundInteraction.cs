using DiasGames.Components;
using UnityEngine;

namespace _Project.Logic.Character
{
    public class DeathSoundInteraction : MonoBehaviour
    {
        [SerializeField] private Health _health;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private AudioClip _fall;
        [SerializeField] private AudioClip _catch;

        private void Start() => 
            _health.OnDead += PlaySound;

        private void OnDestroy() => 
            _health.OnDead -= PlaySound;

        private void PlaySound(DeathReason deathReason) =>
            _audioSource.PlayOneShot(deathReason is DeathReason.Health
                ? _fall
                : _catch);
    }
}
