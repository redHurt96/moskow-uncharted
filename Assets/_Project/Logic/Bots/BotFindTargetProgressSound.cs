using UnityEngine;

namespace _Project.Logic.Bots
{
    public class BotFindTargetProgressSound : MonoBehaviour
    {
        [SerializeField] private BotHandler _botHandler;
        [SerializeField] private AudioSource _findAudioSource;
        [SerializeField] private float _minVolume = .5f;
        [SerializeField] private float _maxVolume = 1f;

        private void Update()
        {
            _findAudioSource.mute = Mathf.Approximately(_botHandler.Bot.ObserveTargetProgress, 0f);
            _findAudioSource.volume = Mathf.Lerp(_minVolume, _maxVolume, _botHandler.Bot.ObserveTargetProgress);
        }
    }
}