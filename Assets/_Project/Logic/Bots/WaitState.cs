using AI.FluentFiniteStateMachine;

namespace _Project.Logic.Bots
{
    internal class WaitState : State<Bot>, IEnterState
    {
        public WaitState(Bot bot) : base(bot) {}

        public void OnEnter() => 
            _context.StartWaiting();
    }
}