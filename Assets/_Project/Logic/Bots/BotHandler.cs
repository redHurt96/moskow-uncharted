using AI.FluentFiniteStateMachine;
using UnityEngine;

namespace _Project.Logic.Bots
{
    public class BotHandler : MonoBehaviour
    {
        public Bot Bot => _bot;
        
        [SerializeField] private Bot _bot;
        
        private StateMachine _stateMachine;

        private void Start() =>
            _stateMachine = new StateMachine()
                .AddStates(
                    new PatrolState(_bot),
                    new WaitState(_bot),
                    new FindState(_bot))
                .AddTransitions(
                    new Transition(typeof(PatrolState), typeof(FindState), _bot.FindTarget),
                    new Transition(typeof(PatrolState), typeof(WaitState), _bot.ReachPoint),
                    new Transition(typeof(WaitState), typeof(PatrolState), _bot.WaitEnough),
                    new Transition(typeof(WaitState), typeof(FindState), _bot.FindTarget))
                .Run();

        private void Update() => 
            _stateMachine.Update();
    }
}