using DiasGames.Components;
using DiasGames.Controller;
using UnityEngine;
using static UnityEngine.Physics;
using static UnityEngine.Vector3;

namespace _Project.Logic.Bots
{
    public class BotTargetObserver : MonoBehaviour
    {
        [SerializeField] private BotHandler _botHandler;

        [SerializeField, ReadOnly] private GameObject _gameObject; 
        
        private Transform _target;
        private BotTargetObservePoint _viewPoint;
        
        private Bot _bot => _botHandler.Bot;

        private void Start()
        {
            _viewPoint = FindObjectOfType<BotTargetObservePoint>();
            _target = FindObjectOfType<Health>().transform;
        }

        private void Update()
        {
            Debug.DrawLine(transform.position, _viewPoint.Position, Color.green);
            
            if (CloseEnough()
                && InFrontOfTarget()
                && TargetInSight())
                _botHandler.Bot.DetectTarget(_target);
            else
                _botHandler.Bot.LostTarget();
        }

        private bool CloseEnough() => 
            Distance(transform.position, _target.position) <= _bot.TargetDetectDistance;

        private bool InFrontOfTarget() => 
            Angle(transform.forward, (_target.position - transform.position).normalized) < _bot.TargetDetectAngle;

        private bool TargetInSight()
        {
            bool targetInSight = Raycast(transform.position, (_viewPoint.Position - transform.position).normalized, out RaycastHit hit)
                                   && hit.transform.TryGetComponent(out CSPlayerController controller);

            if (hit.transform != null)
                _gameObject = hit.transform.gameObject;
            else
                _gameObject = null;
            
            return targetInSight;
        }
    }
}