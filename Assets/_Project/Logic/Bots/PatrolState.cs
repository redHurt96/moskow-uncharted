using AI.FluentFiniteStateMachine;

namespace _Project.Logic.Bots
{
    internal class PatrolState : State<Bot>, IEnterState, IExitState
    {
        public PatrolState(Bot context) : base(context) {}

        public void OnEnter() => 
            _context.MoveToCurrentPoint();

        public void OnExit() => 
            _context.Stop();
    }
}