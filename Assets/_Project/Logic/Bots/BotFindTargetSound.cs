using UnityEngine;

namespace _Project.Logic.Bots
{
    public class BotFindTargetSound : MonoBehaviour
    {
        [SerializeField] private BotHandler _botHandler;
        [SerializeField] private AudioSource _findAudioSource;

        private void Update()
        {
            if (_botHandler.Bot.ObserveTargetProgress >= 1f)
            {
                _findAudioSource.Play();
                Destroy(this);
            }
        }
    }
}