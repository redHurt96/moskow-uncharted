using System.Collections.Generic;
using UnityEngine;

namespace _Project.Logic.Bots
{
    internal class ObserveUiStack : MonoBehaviour
    {
        private readonly List<BotObserveProgressUI> _stack = new();

        public bool Contains(BotObserveProgressUI botObserveProgressUI) => 
            _stack.Contains(botObserveProgressUI);

        public void Add(BotObserveProgressUI botObserveProgressUI)
        {
            _stack.Add(botObserveProgressUI);
            botObserveProgressUI.transform.parent = transform;
        }

        public void Remove(BotObserveProgressUI botObserveProgressUI)
        {
            _stack.Remove(botObserveProgressUI);
            botObserveProgressUI.transform.parent = transform.parent;
        }
    }
}