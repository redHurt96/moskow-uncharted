using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.Animator;

namespace _Project.Logic.Bots
{
    public class BotAnimatorAdapter : MonoBehaviour
    {
        [SerializeField] private BotHandler _botHandler;
        [SerializeField] private NavMeshAgent _navMeshAgent;
        [SerializeField] private Animator _animator;
        
        private static readonly int _speed = StringToHash("Speed");
        private static readonly int _aim = StringToHash("Aim");

        private void Update()
        {
            if (_botHandler.Bot.ObserveTargetProgress >= 1f)
            {
                _animator.SetTrigger(_aim);
            }
            else
            {
                float speed = _navMeshAgent.velocity.magnitude > .2f ? 1f : 0f;
                _animator.SetFloat(_speed, speed);    
            }
        }
    }
}