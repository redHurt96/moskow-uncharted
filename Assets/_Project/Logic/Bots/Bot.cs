using System;
using DG.Tweening;
using DiasGames.Components;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.Mathf;
using static UnityEngine.Time;
using static UnityEngine.Vector3;

namespace _Project.Logic.Bots
{
    [Serializable]
    public class Bot
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        [SerializeField] private Transform _transform;
        [SerializeField] private Transform[] _points;
        [SerializeField] private float _standAtPointTime;
        [SerializeField] private float _stoppingDistanceThreshold = 1f;
        [SerializeField] private float _targetDetectDistance;
        [SerializeField] private float _targetDetectAngle;
        [SerializeField] private float _targetFindSpeed;

        [SerializeField, ReadOnly] private float _observeTargetProgress;

        private Health _target;
        private int _currentPointIndex;
        private float _reachPointTime;

        private Vector3 CurrentPoint => _points[_currentPointIndex].position;
        public float TargetDetectDistance => _targetDetectDistance;
        public float TargetDetectAngle => _targetDetectAngle;
        public float ObserveTargetProgress => _observeTargetProgress;

        public bool FindTarget() => 
            _observeTargetProgress >= 1f;

        public void MoveToCurrentPoint()
        {
            if (_points == null || _points.Length == 0)
                return;
            
            _navMeshAgent.isStopped = false;
            _navMeshAgent.SetDestination(CurrentPoint);
        }

        public bool ReachPoint()
        {
            if (_points == null || _points.Length == 0)
                return false;
            
            return Distance(_transform.position, CurrentPoint) <= _stoppingDistanceThreshold;
        }

        public bool WaitEnough() => 
            _reachPointTime + _standAtPointTime < time;

        public void Stop() => 
            _navMeshAgent.isStopped = true;

        public void StartWaiting()
        {
            _reachPointTime = time;
            _currentPointIndex = _currentPointIndex == _points.Length - 1
                ? 0
                : _currentPointIndex + 1;
        }

        public void SendEndGameToTarget() => 
            _target.CatchByEnemy();

        public void DetectTarget(Transform transform)
        {
            _observeTargetProgress = Clamp01(_observeTargetProgress + _targetFindSpeed * deltaTime);
            _target ??= transform.GetComponentInParent<Health>();
        }

        public void LostTarget()
        {
            _observeTargetProgress = Clamp01(_observeTargetProgress - _targetFindSpeed * deltaTime);
            _target = null;
        }

        public void RotateToTarget() => 
            _transform.forward = (_target.transform.position - _transform.position).normalized;
    }
}