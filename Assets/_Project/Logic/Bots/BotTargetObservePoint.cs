using UnityEngine;

namespace _Project.Logic.Bots
{
    public class BotTargetObservePoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;
    }
}