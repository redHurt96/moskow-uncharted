using DiasGames.Components;
using UnityEngine;
using static UnityEngine.Resources;

namespace _Project.Logic.Bots
{
    public class BotObserveProgressUIController : MonoBehaviour
    {
        [SerializeField] private BotHandler _botHandler;
        [SerializeField] private Renderer _renderer;

        private BotUIParent _parent;
        private Health _target;
        private Camera _camera;
        private BotObserveProgressUI _ui;

        private void Start()
        {
            _parent ??= FindObjectOfType<BotUIParent>();
            _target ??= FindObjectOfType<Health>();
            _camera ??= FindObjectOfType<Camera>();
            _ui = Instantiate(Load<BotObserveProgressUI>(nameof(BotObserveProgressUI)), _parent.transform);
        }

        private void Update()
        {
            _ui.SetActive(_botHandler.Bot.ObserveTargetProgress > 0f);
            
            if (_botHandler.Bot.ObserveTargetProgress > 0f)
            {
                if (_renderer.isVisible)
                    _ui.UpdateFromVisibleBot(_camera.WorldToScreenPoint(transform.position + Vector3.up), _botHandler.Bot.ObserveTargetProgress);
                else
                    _ui.UpdateFromBotBehindCamera(_botHandler.Bot.ObserveTargetProgress);
            }
        }
    }
}