using AI.FluentFiniteStateMachine;

namespace _Project.Logic.Bots
{
    internal class FindState : State<Bot>, IEnterState
    {
        public FindState(Bot bot) : base(bot) {}

        public void OnEnter()
        {
            _context.RotateToTarget();
            _context.SendEndGameToTarget();
        }
    }
}