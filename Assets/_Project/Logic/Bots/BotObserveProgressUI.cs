using UnityEngine;
using UnityEngine.UI;

namespace _Project.Logic.Bots
{
    internal class BotObserveProgressUI : MonoBehaviour
    {
        [SerializeField] private Image _fill;
        
        private RectTransform _transform;
        private ObserveUiStack _observeUiStack;

        private void Start()
        {
            _observeUiStack = FindObjectOfType<ObserveUiStack>();
            _transform = transform as RectTransform;
        }

        public void SetActive(bool isActive) => 
            gameObject.SetActive(isActive);

        public void UpdateFromBotBehindCamera(float progress)
        {
            if (!_observeUiStack.Contains(this))
                _observeUiStack.Add(this);

            _fill.fillAmount = progress;
        }

        public void UpdateFromVisibleBot(Vector3 screenPoint, float progress)
        {
            if (_observeUiStack.Contains(this))
                _observeUiStack.Remove(this);
            
            _fill.fillAmount = progress;
            _transform.localPosition = screenPoint - new Vector3(Screen.width, Screen.height, 0f) / 2f;
        }
    }
}