using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.Animator;

namespace _Project.Logic.Bots
{
    public class CrowdPersonAnimatorAdapter : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _navMeshAgent;
        [SerializeField] private Animator _animator;
        
        private static readonly int _speed = StringToHash("Speed");

        private void Update()
        {
            float speed = _navMeshAgent.velocity.magnitude > .2f ? 1f : 0f;
            _animator.SetFloat(_speed, speed);    
        }
    }
}