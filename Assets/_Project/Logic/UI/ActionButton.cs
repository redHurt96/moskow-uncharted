using UnityEngine;
using UnityEngine.UI;

namespace _Project.Logic.UI
{
    [RequireComponent(typeof(Button))]
    public abstract class ActionButton : MonoBehaviour
    {
        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnClick);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        protected abstract void OnClick();
    }
}