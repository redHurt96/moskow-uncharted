using UnityEngine;

namespace _Project.Logic.UI
{
    public class PagesChanger : ActionButton
    {
        [SerializeField] private GameObject _from;
        [SerializeField] private GameObject _to;
        
        protected override void OnClick()
        {
            _from.SetActive(false);
            _to.SetActive(true);
        }
    }
}
