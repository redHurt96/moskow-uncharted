using EasyTransition;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace _Project.Logic.UI
{
    public class LoadSceneWithTransitionButton : ActionButton
    {
        [SerializeField] private int _sceneBuildIndex;
        
        protected override void OnClick()
        {
            FindObjectOfType<TransitionManager>()
                .LoadScene(_sceneBuildIndex, "RectangleGrid", 0f);

            foreach (Button button in FindObjectsOfType<Button>()) 
                button.enabled = false;
        }
    }
}