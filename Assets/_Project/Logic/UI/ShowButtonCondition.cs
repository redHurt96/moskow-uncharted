using System;
using UnityEngine;

namespace _Project.Logic.UI
{
    public abstract class ShowButtonCondition : MonoBehaviour
    {
        protected abstract Func<bool> Condition { get; }

        [SerializeField] private GameObject _button;

        private void OnEnable() => 
            _button.SetActive(Condition());
    }
}