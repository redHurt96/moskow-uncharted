using static UnityEngine.Application;

namespace _Project.Logic.UI
{
    public class QuitButton : ActionButton
    {
        protected override void OnClick() => Quit();
    }
}