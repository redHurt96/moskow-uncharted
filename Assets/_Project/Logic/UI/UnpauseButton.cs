using DiasGames.Components;

namespace _Project.Logic.UI
{
    public class UnpauseButton : ActionButton
    {
        private PauseComponent _pauseComponent;

        protected override void OnClick()
        {
            _pauseComponent ??= FindObjectOfType<PauseComponent>();
            _pauseComponent.OnPause(false);
        }
    }
}