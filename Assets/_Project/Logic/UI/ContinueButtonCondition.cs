using System;
using static _Project.Logic.Saving.SaveManager;

namespace _Project.Logic.UI
{
    public class ContinueButtonCondition : ShowButtonCondition
    {
        protected override Func<bool> Condition => () => HasSave;
    }
}