using EasyTransition;
using static _Project.Logic.Saving.SaveManager;

namespace _Project.Logic.UI
{
    public class LoadNewGameButton : ActionButton
    {
        protected override void OnClick()
        {
            ClearSave();
            FindObjectOfType<TransitionManager>().LoadScene(1, "RectangleGrid", 0f);
        }
    }
}