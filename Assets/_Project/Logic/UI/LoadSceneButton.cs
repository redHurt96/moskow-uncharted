using UnityEngine;
using static UnityEngine.SceneManagement.SceneManager;

namespace _Project.Logic.UI
{
    public class LoadSceneButton : ActionButton
    {
        [SerializeField] private int _sceneBuildIndex;
        
        protected override void OnClick() => LoadScene(_sceneBuildIndex);
    }
}