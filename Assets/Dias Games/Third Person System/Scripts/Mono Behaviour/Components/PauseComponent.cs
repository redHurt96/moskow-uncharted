using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;
using static UnityEngine.CursorLockMode;

namespace DiasGames.Components
{
    public class PauseComponent : MonoBehaviour
    {
        [SerializeField] private GameObject pauseMenu;
        [SerializeField] private GameObject hud;
        
        [Space]
        [SerializeField] private float _fadeDuration;
        [SerializeField] private Volume _volume;
        [SerializeField] private Volume _globalVolume;
        
        private bool _isPaused;
        private CursorLockMode lockMode;
        private bool visible;
        private Coroutine _routine;

        private void Start()
        {
            visible = Cursor.visible;
            lockMode = Cursor.lockState;
        }

        private void OnDestroy()
        {
            Time.timeScale = 1f;
            BreakRoutine();
        }

        private void OnPause(InputValue value)
        {
            if (value.isPressed)
                OnPause(!_isPaused);
        }

        public void OnPause(bool paused)
        {
            _isPaused = paused;

            Time.timeScale = _isPaused ? 0f : 1f;
            
            pauseMenu.SetActive(_isPaused);
            hud.SetActive(!_isPaused);

            Cursor.visible = paused || visible;
            Cursor.lockState = paused ? None : lockMode;

            BreakRoutine();
            
            if (_isPaused) 
                _routine = StartCoroutine(FadeIn());
            else
                _routine = StartCoroutine(FadeOut());
        }

        private void BreakRoutine()
        {
            if (_routine != null)
            {
                StopCoroutine(_routine);
                _routine = null;
            }
        }

        private IEnumerator FadeIn()
        {
            float time = 0f;

            while (time < _fadeDuration)
            {
                _volume.weight = Mathf.Lerp(0f, 1f, time / _fadeDuration);
                _globalVolume.weight = Mathf.Lerp(1f, 0f, time / _fadeDuration);
                time += Time.unscaledDeltaTime;

                yield return null;
            }
            
            _volume.weight = 1f;
        }
        
        private IEnumerator FadeOut()
        {
            float time = 0f;

            while (time < _fadeDuration)
            {
                _volume.weight = Mathf.Lerp(1f, 0f, time / _fadeDuration);
                _globalVolume.weight = Mathf.Lerp(0f, 1f, time / _fadeDuration);
                time += Time.deltaTime;

                yield return null;
            }
            
            _volume.weight = 0f;
        }
    }
}